import Vue from 'vue';
import { BootstrapVue } from 'bootstrap-vue';
import VModal from 'vue-js-modal';
import moment from 'moment';
import App from './App.vue';
import router from './router';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(BootstrapVue);
Vue.use(VModal);
Vue.config.productionTip = false;

// eslint-disable-next-line consistent-return
Vue.filter('formatDate', (value) => {
  if (value) {
    return moment(String(value)).format('LLL');
  }
});

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
