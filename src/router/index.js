import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import Home from '../views/Home.vue';
import Register from '../components/Register.vue';
import Logout from '../components/Logout.vue';
import Account from '../components/Account.vue';
import Dashboard from '../components/Dashboard.vue';
import BasicHealthcheck from '../components/BasicHealthcheck.vue';
import ComplexHealthcheck from '../components/ComplexHealthcheck.vue';
import ComponentInfo from '../components/ComponentInfo.vue';

Vue.use(VueRouter);

const token = localStorage.getItem('user-token');
if (token) {
  axios.defaults.headers.common.Authorization = token;
}

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard,
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: () => import(/* webpackChunkName: "about" */ '../views/Dash.vue'),
  },
  {
    path: '/signup',
    name: 'Register',
    component: Register,
  },
  {
    path: '/account',
    name: 'Account',
    component: Account,
  },
  {
    path: '/logout',
    name: 'Logout',
    component: Logout,
  },
  {
    path: '/basic',
    name: 'BasicHealthcheck',
    component: BasicHealthcheck,
  },
  {
    path: '/complex',
    name: 'ComplexHealthcheck',
    component: ComplexHealthcheck,
  },
  {
    path: '/component',
    name: 'ComponentInfo',
    component: ComponentInfo,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
