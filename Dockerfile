# syntax = docker/dockerfile:experimental

FROM node:16.15.0-buster AS frontbuild

ARG SERVER_API_URL=${SERVER_API_URL:-""}

WORKDIR /tmp
COPY package.json /tmp
RUN npm install -g npm@latest \
 && npm install

WORKDIR /home/node
COPY . .

RUN sed -ir "s/^[#]*\s*SERVER_API_URL=.*/SERVER_API_URL=${SERVER_API_URL}/" .env \
 && cat .env && sleep 10 \
 && if [ -d build ]; then mv build /tmp; fi \
 && if [ -d dist ]; then mv dist /tmp; fi \
 && if [ -d node_modules ]; then mv node_modules ..; fi \
 && ln -s /tmp/node_modules .

RUN npm run build

FROM nginx:1.21.6-alpine

ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL
ARG VERSION
ARG SERVER_NAME=${SERVER_NAME:-app-frontend}

# http://label-schema.org/rc1/
LABEL anon.fic.udc.es.schema-version="1.0.0-rc1" \
      anon.fic.udc.es.vendor="Michael Añon" \
      anon.fic.udc.es.maintainer="michael.varela@udc.es" \
      anon.fic.udc.es.name=$SERVER_NAME \
      anon.fic.udc.es.version=$VERSION \
      anon.fic.udc.es.description="frontend to main api rest" \
      anon.fic.udc.es.build-date=$BUILD_DATE \
      anon.fic.udc.es.vcs-url=$VCS_URL \
      anon.fic.udc.es.vcs-ref=$VCS_REF

ENV PORT=${PORT}
ENV APP_DIR=/srv/app
ENV APP_LOG=/srv/log
ENV BASIC_LOG_DIR=${BASIC_LOG_DIR:-${APP_LOG}"/app"}
WORKDIR ${APP_DIR}

ENV BASIC_LOGGER_LEVEL=${BASIC_LOGGER_LEVEL:-INFO}

EXPOSE 80

COPY --from=frontbuild /home/node/dist /usr/share/nginx/html

# nginx configuration
COPY conf/nginx.conf /etc/nginx/

